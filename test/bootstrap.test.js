/**
 * @description: This file is userfull when you want to execute some code before and after runnin your tests
 */

var Sails = require('sails'),
  sails;

before(function(done) {
  Sails.lift({

    log: {
      level: 'error'
    },
    connections:{
      mongodb: {
        adapter   : 'sails-mongo',
        url: 'mongodb://localhost:27017/[you DB]',
        schema: true
      }
    },

    models:{
      migrate:'drop'
    },
    middleware: {
      order: [
        'startRequestTimer',
        'cookieParser',
        'session',
        //   'myRequestLogger',
        'bodyParser',
        'handleBodyParserError',
        'compress',
        'methodOverride',
        //   'poweredBy',
        //   '$custom',
        'router',
        'www',
        //   'favicon',
        '404',
        '500'
      ]
    },
    port: 1330,
   /* environment:'test'*/
    environment:'development'
  }, function(err, server) {
    sails = server;
    if (err) return done(err);

    done(err, sails);
  });
});

after(function(done) {
  Sails.lower(done);
});
